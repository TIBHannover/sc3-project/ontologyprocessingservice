# Ontology Processing Service

Flask wrapper to create the ontology processing service

# preparation
run `sh initializeEnv.sh` -> creates an .env file

IMPORTANT: 
You need to have a version of `owlapi-cli.jar`

You can download the repo at
https://gitlab.com/TIBHannover/sc3-project/dr-owl-api

and follow the installation descriptions in the README.md 

i.e., On linux ./build_standalone.sh, this will build the jar file 'owlapi-cli.jar'

and copy it into this folder here.

# start proxy server
`python3 flask-proxy.py'

Now you should be able to open the browser and see under http://localhost:12462/ the following message "Ontology Data Processing Service"

You can test if everything works by going to http://localhost:12462/testing
this will lunch the owlapi-cli and validate an example ontology, the result is a json object

that should like this: 

{

  "imports": "successful",
   
  "parser": "successful",
   
  "validation": "failed",
   
  "validationErrors": [
    Some error
  ]
  
}



