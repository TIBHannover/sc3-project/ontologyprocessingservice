@echo off
echo "CALLING WITHOUT PARAMETERS"
java -jar owlapi-cli.jar
echo "DONE"

echo "TASK 1"
java -jar owlapi-cli.jar -file example.ttl -task validate
type output.txt
del output.txt

echo ""
echo "TASK 2"
java -jar owlapi-cli.jar -file example.ttl -task pre-init
type output.txt
del output.txt
del example.ttl.ttl

echo ""
echo "TASK 3"
java -jar owlapi-cli.jar -file example.ttl -task jsonModel
type default.json
del default.json
