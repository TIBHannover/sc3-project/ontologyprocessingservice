'use strict';

var myArgs = process.argv.slice(2);
const VOWLParser= require("./Implementation/VOWLParser");
const fs = require('fs');

fs.readFile(myArgs[0], (err, data) =>{
  if (err) throw err;
  const vowlJson= JSON.parse(data);
 
  const parser= new VOWLParser();
  parser.setInput(vowlJson);
  parser.execute();
  
  const result=parser.m_resultingData;
  const dataToWrite=JSON.stringify(result, ' ', 2 , null);
  // fs.writeFileSync('student-2.json', data);
  let outputFileName= myArgs[0].substring(0,myArgs[0].length-5);
  outputFileName+="_RRM.json";
  fs.writeFileSync(outputFileName, dataToWrite);
  

});
