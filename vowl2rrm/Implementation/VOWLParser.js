const ResourceRelationModel=require("./Models/ResourceRelationModel");
const Resource=require("./Models/Resource");
const Relation=require("./Models/Relation");

module.exports = function (  ){
  
  const vowlParser={
    m_inputData:undefined,
    m_resultingData:undefined
  };
  
  vowlParser.setInput=function(inputJSON){
    vowlParser.m_inputData=inputJSON;
  };
  vowlParser.execute=function(){
    vowlParser.__run__();
  };
  
  vowlParser.__run__ = function() {
    const m = new ResourceRelationModel();
    const processedData = vowlParser._preProcess();
    vowlParser._processData(m, processedData);
    vowlParser._postProcess();
    // add meta information object
    m.modelAsJsonObject.metaInformation={};
    vowlParser._addPrefixList(m.modelAsJsonObject.metaInformation);
    vowlParser._addMetaInformation(m.modelAsJsonObject.metaInformation);
    
  };
  
  vowlParser._addPrefixList=function(meta){
    meta.prefixList={shortToLong:{}, longToShort:{}};
    // optional chaining
    if (this.m_inputData && this.m_inputData.header && this.m_inputData.header.prefixList){
      const shortToLong=this.m_inputData.header.prefixList;
      if (shortToLong.hasOwnProperty('')){
        // this is an artifact from owl2vowl
        delete shortToLong[''];
        meta.prefixList.shortToLong=shortToLong;
        // reverse the values
        const longToShort={};
        for (const name in shortToLong){
          if (shortToLong.hasOwnProperty(name)){
            const value=shortToLong[name];
            longToShort[value]=name;
          }
        }
        meta.prefixList.longToShort=longToShort;
      }
    }else{
      console.log("NO PREFIX MAP FOUND")
    }
  };
  vowlParser._addMetaInformation=function (meta){
    meta.metaDescriptions={};
    if(this.m_inputData && this.m_inputData.header){
      vowlParser.__fetchProperty("title", meta.metaDescriptions);
      vowlParser.__fetchProperty("iri", meta.metaDescriptions);
      vowlParser.__fetchProperty("version", meta.metaDescriptions);
      vowlParser.__fetchProperty("author", meta.metaDescriptions);
      vowlParser.__fetchProperty("description", meta.metaDescriptions);
      vowlParser.__fetchProperty("labels", meta.metaDescriptions);
      vowlParser.__fetchProperty("other", meta.metaDescriptions);
    }
  };
  
  vowlParser.__fetchProperty=function(selector, dest){
    // we do previously the check if that thing exist
    const vowlHeader=this.m_inputData.header;
    if (vowlHeader[selector]){
      dest[selector]=vowlHeader[selector];
    }
    
    
  };
  
  vowlParser.__corretRelationTypes=function(inputArray){
    function mapValue(relationType){
      if (relationType==="owl:datatypeProperty") {
        return "owl:DatatypeProperty";
      }
      if (relationType==="owl:objectProperty") {
        return "owl:ObjectProperty";
      }
      return relationType
    }
    for (let i=0;i<inputArray.length;i++){
      inputArray[i]=mapValue(inputArray[i]);
    }
  };
  
  vowlParser._postProcess=function(){
    // post processing deletes some not required fields in the objects;
    const resources=vowlParser.m_resultingData.resources;
    resources.forEach(item=>{
      delete item.prefixMapperL2S;
    });
  
    const relations=vowlParser.m_resultingData.relations;
    relations.forEach(item=>{
      
      // relations type transformer for the frontend
      // TODO : this is hard coded to accept owl:datatypeProperty vs owl:DatatypeProperty
      vowlParser.__corretRelationTypes(item.type);
      
      delete item.prefixMapperL2S;
      delete item.domains;
      delete item.ranges;
    })
    
  };
  
  vowlParser._preProcess = function()  {
    // we merge the vowl json data ;
    const classArray = this.m_inputData.class;
    let propArray = []
    if(this.m_inputData.property){
      propArray = this.m_inputData.property;
    }
    // how to merge that;
    let resourceMap = new Map(classArray.map(d => [d.id, d]));
    let relationMap = new Map(propArray.map(d => [d.id, d]));
    
    const cAttr = this.m_inputData.classAttribute;
    let pAttr = []
    if(this.m_inputData.propertyAttribute){
      pAttr = this.m_inputData.propertyAttribute;
    }
    
    cAttr.forEach(item => {
      item.type = resourceMap.get(item.id).type;
    });
    
    // now use the new attributes array as map input
    resourceMap = new Map(cAttr.map(d => [d.id, d]));
    
    cAttr.forEach(item => {
      if (item.superClasses) {
        for (let i = 0; i < item.superClasses.length; i++) {
          if(resourceMap.get(item.superClasses[i])){
            item.superClasses[i] = resourceMap.get(item.superClasses[i]).iri;
          }else {
            console.warn("No super class was found for IRI: " + item.iri);
          }
        }
      }
      if (item.equivalent) {
        for (let i = 0; i < item.equivalent.length; i++) {
          if(resourceMap.get(item.equivalent[i])){
            item.equivalent[i] = resourceMap.get(item.equivalent[i]).iri;
          }else {
            console.warn("No equivalent was found for IRI: " + item.iri);
          }
        }
      }
    });
    
    
    
    pAttr.forEach(item => {
      item.type = relationMap.get(item.id).type;
      if (item.domain) {
        if(resourceMap.get(item.domain)){
          // overwrite it to be an uri;
          item.domain = resourceMap.get(item.domain).iri;
        }else {
          console.warn("No domain was found for IRI: " + item.iri);
        }
      }
      if (item.range) {
        // overwrite it to be an uri;
        const rangeItem=resourceMap.get(item.range);
        if (!rangeItem){
          // console.log(item);
          // console.log(resourceMap)
        }else {
          if(resourceMap.get(item.range)){
            item.range = resourceMap.get(item.range).iri;
          }else {
            console.warn("No range was found for IRI: " + item.iri);
          }
        }
      }
    });

    // now use the new attributes array as map input
    relationMap = new Map(pAttr.map(d => [d.id, d]));
    pAttr.forEach(item => {
      if (item.subproperty) {
        for (let i = 0; i < item.subproperty.length; i++) {
          if(relationMap.get(item.subproperty[i])){
            item.subproperty[i] = relationMap.get(item.subproperty[i]).iri;
          }else {
            console.warn("No sub property found for IRI: " + item.iri);
          }
        }
      }
      if (item.equivalent) {
        for (let i = 0; i < item.equivalent.length; i++) {
          if(relationMap.get(item.equivalent[i])){
            item.equivalent[i] = relationMap.get(item.equivalent[i]).iri;
          }else {
            console.warn("No equivalent was found for IRI: " + item.iri);
          }
        }
      }
      if (item.inverse) {
        if(relationMap.get(item.inverse)){
          item.inverse = relationMap.get(item.inverse).iri;
        }else {
          console.warn("No inverse was found for IRI: " + item.iri);
        }
      }
    });
    return { resources: cAttr, relations: pAttr };
   };
  
   vowlParser._processData=function(model, dataObject) {
     vowlParser.__processResources(model, dataObject);
     vowlParser.__processRelations(model, dataObject);
     vowlParser.m_resultingData = model.modelAsJsonObject;
  };
  //
  
  vowlParser.__processResources=function(model, dataObject) {
    dataObject.resources.forEach(resource => {
      // if there is no IRI FOR THIS ONE check the type of VOWL;
      if (!resource.iri) {
        resource.__resourceIdentifier = resource.type; // kindOF processing type as IRI (e.g. OWL:THING or so)
      } else {
        resource.__resourceIdentifier = resource.iri;
      }
      
      const modelResource = new Resource();
      // fetch info from it;
      modelResource.resourceIdentifier(resource.__resourceIdentifier);
      modelResource.resourceURI = resource.iri;
      modelResource.addSemanticType(resource.type);
  
      vowlParser.__processLabels(resource, modelResource);
      vowlParser.__processComments(resource, modelResource);
      vowlParser.__processResourceAxioms(resource, modelResource);
      model.addResource(modelResource);
    });
  };
  
  vowlParser.__processRelations=function(model, dataObject) {
    dataObject.relations.forEach(relation => {
      if (!relation.iri) {
        relation.identifier = relation.type; // kindOF processing type as IRI (e.g. OWL:THING or so)
      } else {
        relation.identifier = relation.iri;
      }
      
      // VOWL provides axioms directly as relations, here ignore.
      if (relation.type.toLowerCase() === "rdfs:SubClassOf".toLowerCase()) {
        return;
      }
      if (relation.type.toLowerCase()==="owl:allValuesFrom".toLowerCase()){
        return;
      }
      if (relation.type.toLowerCase()==="owl:disjointWith".toLowerCase()){
        return;
      }
      if (relation.type.toLowerCase()==="owl:someValuesFrom".toLowerCase()){
        return;
      }

      const modelRelation = new Relation();
      
      // fetch info from it;
      modelRelation.resourceIdentifier(relation.identifier);
      modelRelation.resourceURI = relation.iri;
      modelRelation.addSemanticType(relation.type);
      vowlParser.__processLabels(relation, modelRelation);
      vowlParser.__processComments(relation, modelRelation);
      vowlParser.__processRelationAxioms(relation, modelRelation);
      vowlParser.__processRelationAttributes(relation, modelRelation);
     
      model.addRelation(modelRelation);
    });
  };
  
  
  
  /** -------------- INTERNAL FUNCTIONS -------------- **/
  vowlParser.__processLabels=function(resource, modelResource) {
    // unroll the labels as rdfs:labels;
    if (resource.label) {
      // label can be an object or a string?
      if (typeof resource.label === "string") {
        modelResource.addAnnotation("rdfs:label", resource.label);
      } else {
        for (const name in resource.label) {
          if (resource.label.hasOwnProperty(name)) {
            if (name !== "IRI-based") {
              modelResource.addAnnotation(
                "rdfs:label",
                resource.label[name],
                name
              );
            }
          }
        }
      }
    }
  };
  
  vowlParser.__processComments=function(resource, modelResource) {
    if (resource.comment) {
      // label can be an object or a string?
      if (typeof resource.comment === "string") {
        modelResource.addAnnotation("rdfs:comment", resource.comment);
      } else {
        for (const name in resource.comment) {
          if (resource.comment.hasOwnProperty(name)) {
            if (name !== "IRI-based") {
              modelResource.addAnnotation(
                "rdfs:comment",
                resource.comment[name],
                name
              );
            }
          }
        }
      }
    }
  };
  
  vowlParser.__processResourceAxioms=function(resource, modelResource) {
    if (resource.equivalent) {
      resource.equivalent.forEach(eq => {
        modelResource.addAxiom(modelResource, "owl:equivalentClass", eq);
      });
    }
    if (resource.superClasses) {
      resource.superClasses.forEach(sc => {
        modelResource.addAxiom(modelResource, "rdfs:subClassOf", sc);
      });
    }
  };
  
  vowlParser.__processRelationAxioms=function(relation, modelResource) {
    //  ignore for now;
  };
  
  vowlParser.__processRelationAttributes=function(relation, modelRelation) {
    // vowl has max one domain and one range ( if not given it will be owl:THING in VOWL )
    if (relation.domain) {
      modelRelation.addDomain(relation.domain);
    }
    if (relation.range) {
      modelRelation.addRange(relation.range);
    }
    
    if (relation.domain && relation.range) {
      modelRelation.addDomainRangePair(relation.domain, relation.range);
    }
  };
  
  
  
  
  
  vowlParser.sayHello=function(){
    console.log("PARSER HELLO");
    
  };
  
  return vowlParser;
};