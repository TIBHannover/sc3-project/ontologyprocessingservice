var Resource= require("./Resource");

module.exports = (function (){
  const Relation=function(){
    Resource.apply(this,arguments);
    var that = this;
    var superIntegrateResource=that.integrateResource;
    
    this.domains = [];
    this.ranges = [];
    this.domainRangePairs = [];
  
  
    this.addRange=function(r) {
      if (this.ranges.indexOf(r) === -1) {
        this.ranges.push(r);
      }
    };
  
    this.addDomain=function(d) {
      this.domains.push(d);
    };
  
    this.addDomainRangePair=function(d, r) {
      this.domainRangePairs.push({ domain: d, range: r });
    };
  
    this.integrateResource=function(src) {
      superIntegrateResource(src);
      src.domains.forEach(dom => {
        if (this.domains.indexOf(dom) === -1) {
          this.domains.push(dom);
        }
      });
      src.ranges.forEach(ran => {
        if (this.ranges.indexOf(ran) === -1) {
          this.ranges.push(ran);
        }
      });
    
      src.domainRangePairs.forEach(pair => {
        this.domainRangePairs.push(pair);
      });
    }
    
  };
  Relation.prototype.constructor = Relation;
  return Relation;
}());
