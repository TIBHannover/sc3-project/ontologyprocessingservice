const validIRI = function( str ) {
  const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
  return urlregex.test(str);
};

module.exports = (function (){
  
  const Resource=function(){
    var that=this;
    this.resourceURI = undefined;
    this.type = [];
    this.annotations = {};
    this.axioms = {};
    this.identifier = undefined;
  
    this.prefixMapperL2S = {
      "http://www.w3.org/2000/01/rdf-schema#": "rdfs"
    };
  
    this.resourceIdentifier=function(val) {
      if (!arguments.length) {
        return this.identifier;
      }
      this.identifier = val;
    };
  
    this.addSemanticType=function(t) {
      if (this.type.indexOf(t) === -1) {
        this.type.push(t);
      }
    };
    
    this.addAnnotation = function(name, value, lang)  {
      // annotations are literals with optional language tags;
    
      let inputName = name;
      if (validIRI(name)) {
        let suffix = name.split("#")[1];
        let pref = "";
        if (!suffix) {
          const tokens = name.split("/");
          suffix = tokens[tokens.length - 1];
          for (let i = 0; i < tokens.length - 1; i++) {
            pref += tokens[i];
          }
          pref += "/";
        } else {
          pref = name.split("#")[0];
          pref += "#";
        }
      
        const prName = this.prefixMapperL2S[pref];
        if (prName) {
          inputName = prName + ":" + suffix;
        }
      }
      if (!this.annotations[inputName]) {
        this.annotations[inputName] = {};
      }
    
      let languageSelector = "default";
      if (lang && lang !== "undefined") {
        languageSelector = lang;
      }
      const tm = this.annotations[inputName];
      if (!tm[languageSelector]) {
        tm[languageSelector] = []; // array of strings for given language
      }
      tm[languageSelector].push(value);
    };
  
    this.addAxiom=function(subject, axiom, object) {
      // subject for now ignored;
      if (!this.axioms.hasOwnProperty(axiom)) {
        this.axioms[axiom] = [];
      }
      this.axioms[axiom].push(object);
    };
  
    this.integrateResource = src => {
      // adds stuff to this resource;
      // semantic type?
      src.type.forEach(t => {
        this.addSemanticType(t); // no need for checking, it is done in the function itself;
      });
      // annotations is an object;
      for (const name in src.annotations) {
        if (src.annotations.hasOwnProperty(name)) {
          const annoType = src.annotations[name];
          // check if this name exist in this ;
          if (!this.annotations[name]) {
            this.annotations[name] = {};
          }
          //
          for (const langType in annoType) {
            if (annoType.hasOwnProperty(langType)) {
              // check if this object has it;
              const value = annoType[langType]; // value is an array
              const tm = this.annotations[name];
              if (!tm[langType]) {
                tm[langType] = []; // array of strings for given language
              }
              value.forEach(str => {
                if (tm[langType].indexOf(str) === -1) {
                  tm[langType].push(str);
                }
              });
            }
          }
        }
      }
    
      // check for axioms merging
      for (const name in src.axioms) {
        if (src.axioms.hasOwnProperty(name)) {
          if (!this.axioms[name]) {
            this.axioms[name] = [];
          }
          // get array of axioms holders;
          const axAr = src.axioms[name];
          axAr.forEach(ax => {
            // check if exists;
            if (this.axioms[name].indexOf(ax) === -1) {
              this.axioms[name].push(ax);
            }
          });
        }
      }
    }
    
  };
  
  Resource.prototype.constructor = Resource;
  return Resource;
}());
