FROM openjdk:11


#PREPARE
RUN apt-get update 
RUN apt-get install -y --no-install-recommends git ca-certificates 
RUN apt-get install -y python3 python3-pip nodejs
RUN  rm -rf /var/lib/apt/lists/*

WORKDIR /ontologyProcessing
ADD requirements.txt /ontologyProcessing
ADD . /ontologyProcessing


# Install requirements
RUN \
  pip3 install --upgrade pip && \
  pip3 install --no-cache -r requirements.txt && \
  rm -rf ~/.cache/

EXPOSE 12463
CMD python3 flask-proxy.py









