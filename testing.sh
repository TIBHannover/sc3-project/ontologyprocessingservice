#!/bin/bash

echo "CALLING WITHOUT PARAMETERS"
java -jar owlapi-cli.jar && 
echo "DONE"

echo "TASK 1"
java -jar owlapi-cli.jar -file example.ttl -task validate
cat output.txt
rm output.txt

echo ""
echo "TASK 2"
java -jar owlapi-cli.jar -file example.ttl -task pre-init
cat output.txt
rm output.txt
rm example.ttl.ttl

echo ""
echo "TASK 3"
java -jar owlapi-cli.jar -file example.ttl -task jsonModel
cat default.json
rm default.json


