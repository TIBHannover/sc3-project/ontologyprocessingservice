#!/usr/bin/env python3
import json
import logging
import os
import subprocess
from dotenv import load_dotenv
from flask import Flask, request
from flask_cors import CORS
from uuid import uuid4
from widoco_documentation import getWidoco_directory

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# some initialization
load_dotenv()  # take environment variables from .env.

app = Flask(__name__)
CORS(app)

app.config["HOST"] = os.environ["FLASK_HOST"] if "FLASK_HOST" in os.environ else '0.0.0.0'
app.config["PORT"] = int(os.environ["FLASK_PORT"]) if "FLASK_PORT" in os.environ else 12463

cli_jar_name = 'owlapi-cli.jar'
owl2vowl_jar_name = 'owl2vowl.jar'
vowl2rrm_script = 'vowl2rrm/main.js'
currentPath = os.getcwd()
target_dir = currentPath + "/temp"


# HELPER FUNCTIONS
def write_temporary_file(temp_id, data_item):
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)

    file_name = 'temp_' + temp_id + '.owl'
    the_file = open(target_dir + '/' + file_name, 'w', encoding="utf-8")
    the_file.write(data_item["ontologyData"])
    the_file.close()


def remove_temporary_file(name):
    if os.path.exists(target_dir + "/" + name):
        os.remove(target_dir + "/" + name)


def get_data_item(data):
    as_string = data.decode('utf-8')
    data_item = json.loads(as_string)
    return data_item


# APP FUNCTIONS

@app.route('/', methods=['GET'])
def welcome():
    print(target_dir)
    return "Ontology Data Processing Service"


@app.route('/testing', methods=['GET'])
def run_example_proxyRequest():
    static_process_call = f"java -jar {cli_jar_name} -file example.ttl -task validate -output ontologyReport.json"
    call = static_process_call.split()
    subprocess.call(call, stdout=open(os.devnull, 'wb'))
    the_file = open('ontologyReport.json', encoding="utf-8", mode='r')
    data = the_file.read()
    the_file.close()
    # remove temp files
    remove_temporary_file('ontologyReport.json')
    return json.loads(data)


@app.route('/getModelFromJsonVOWL', methods=['POST'])
def getModel_from_JSON():
    data_item = get_data_item(request.data)
    data_to_write = json.dumps(data_item['ontologyData'])
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    temp_id = str(uuid4())
    file_name = 'temp_' + temp_id + '.json'
    the_file = open(target_dir + '/' + file_name, 'w')
    the_file.write(data_to_write)
    the_file.close()
    # now process in vowl2rrm parser and return results
    static_process_call = f"sh processVOWLJSON.sh " + file_name
    call = static_process_call.split()
    #     print("CurrentDir:" + os.getcwd())
    #     print(call, flush=True)
    subprocess.call(call, stdout=open(os.devnull, 'wb'))
    # this creates the VOWL FILE
    parser_output = file_name[0:len(file_name) - 5] + "_RRM.json"
    the_file = open(target_dir + "/" + parser_output, encoding="utf-8", mode='r')
    data = the_file.read()
    the_file.close()

    remove_temporary_file(parser_output)
    remove_temporary_file(file_name)

    return json.loads(data)


@app.route('/getModelFromTTL', methods=['POST'])
def getModel_from_TTL():
    data = request.data.decode('utf-8')
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)
    temp_id = str(uuid4())
    file_name = 'temp_' + temp_id + '.owl'
    the_file = open(target_dir + '/' + file_name, 'w')
    the_file.write(data)
    the_file.close()

    output = temp_id + "_result.json"
    static_process_call = f"sh processOntology.sh " + file_name + " " + output
    call = static_process_call.split()
    #     print("CurrentDir:" + os.getcwd())
    #     print(call, flush=True)
    subprocess.call(call, stdout=open(os.devnull, 'wb'))
    # this creates the VOWL FILE
    parser_output = output[0:len(output) - 5] + "_RRM.json"
    the_file = open(target_dir + "/" + parser_output, encoding="utf-8", mode='r')
    data = the_file.read()
    the_file.close()

    remove_temporary_file(parser_output)
    remove_temporary_file(output)
    remove_temporary_file(file_name)

    return json.loads(data)


@app.route('/getComparisonResult', methods=['POST'])
def getComparison_Results():
    print('Inside getComparisonResult')
    data = json.loads(request.data);

    command = 'java -jar robot.jar  diff --left-iri ' + data['first_ontology'] + ' --right-iri ' + data[
        'second_ontology'] + '  --format html';

    call = command.split()
    results = subprocess.run(call, stdout=subprocess.PIPE, stderr=subprocess.PIPE);

    return results.stdout;

@app.route('/getWidocoDocumentation', methods=['POST'])
def getWidoco_Documentation_file():
    file = request.files.get('file')
    response = getWidoco_directory(file)
    return response

@app.route('/getJsonModelVOWL', methods=['POST'])
def getJSON_ModelVOWL():
    data_item = get_data_item(request.data)
    temp_id = str(uuid4())
    file_name = 'temp_' + temp_id + '.owl'
    output = temp_id + "_result.json"
    write_temporary_file(temp_id, data_item)
    static_process_call = f"sh processOntology.sh " + file_name + " " + output

    # adjustments for windows
    # WIN_FLAG=False
    # if os.name == 'nt':
    # WIN_FLAG=True
    if os.name == 'nt':
        print("WE are a win machines")
        static_process_call = f"processOntology_win.bat " + file_name + " " + output

    call = static_process_call.split()
    #     print("CurrentDir:" + os.getcwd())
    #     print(call, flush=True)
    subprocess.call(call, stdout=open(os.devnull, 'wb'))
    # this creates the VOWL FILE
    parser_output = output[0:len(output) - 5] + "_RRM.json"
    the_file = open(target_dir + "/" + parser_output, encoding="utf-8", mode='r')
    data = the_file.read()
    the_file.close()

    remove_temporary_file(parser_output)
    remove_temporary_file(output)
    remove_temporary_file(file_name)

    return json.loads(data)


@app.route('/getJsonModel', methods=['POST'])
def getJSON_Model():
    data_item = get_data_item(request.data)

    temp_id = str(uuid4())
    file_name = 'temp_' + temp_id + '.owl'
    output = temp_id + "_result.json"
    write_temporary_file(temp_id, data_item)

    os.chdir(target_dir)
    static_process_call = f"java -jar ../{cli_jar_name} -file " + file_name + " -task jsonModel -output " + output
    call = static_process_call.split()
    subprocess.call(call)
    results = open(output, encoding="utf-8", mode='r')
    data = results.read()
    results.close()
    returning_model = json.dumps(data)

    # clean up
    remove_temporary_file(file_name)
    remove_temporary_file(output)

    os.chdir('..')
    return data


@app.route('/initializeOntology/', methods=['POST'])
def test_post_request():
    data_item = get_data_item(request.data)

    temp_id = str(uuid4())
    file_name = 'temp_' + temp_id + '.owl'
    output = temp_id + "_result.json"
    write_temporary_file(temp_id, data_item)

    os.chdir(target_dir)
    static_process_call = f"java -jar ../{cli_jar_name} -file " + file_name + " -task pre-init -output " + output
    call = static_process_call.split()
    subprocess.call(call)

    results = open(temp_id + '_result.json', encoding="utf-8", mode='r')
    data = results.read()
    results.close()
    pre_init_result = json.loads(data)

    if pre_init_result['parser'] == "failed":
        remove_temporary_file(file_name)
        remove_temporary_file(output)
        os.chdir('..')
        return data

    # If parsing was successful we run statics on the data and append them later to the results

    # create statistics process;
    # NOTE: pre-init will write a ttl file regardless of the serialization format in order to allow the statistics query
    # statistics_call = "../apache-jena/bin/sparql --data=" + file_name + ".ttl --results=JSON --query=../statsQuery.rq"
    # target_file = open(file_name + "_stats.json", encoding="utf8", mode='w')
    # print(statistics_call)
    # stats_call = statistics_call.split()
    # subprocess.call(stats_call, stdout=target_file)
    # target_file.close()

    # read the data from files and clean up
    # stats_results = open(file_name + '_stats.json', encoding="utf8", mode='r')
    # stats_data = stats_results.read()
    # stats_results.close()

    # read as jsons and attach results
    # stats = json.loads(stats_data)
    # pre_init_result["stats"] = stats
    # returning_data = json.dumps(pre_init_result)

    # clean up
    remove_temporary_file(file_name)
    remove_temporary_file(output)
    # remove_temporary_file(file_name + '_stats.json')
    os.chdir('..')
    return data


if __name__ == '__main__':
    app.run(host=app.config['HOST'], port=app.config['PORT'])
