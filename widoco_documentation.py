import os
import subprocess
from flask import jsonify
import shutil
from werkzeug.utils import secure_filename

currentPath = os.getcwd()


def clean_widoco_document_dir(directory):
    if os.path.exists(directory):
        for filename in os.listdir(directory):
            file_path = os.path.join(directory, filename)
            try:
                # Check if the file_path is a file or a symbolic link
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                # If it's a directory, use shutil.rmtree() to delete it
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print(f'Failed to delete {file_path}. Reason: {e}')


# generates documentation using widoco.jar
def getWidoco_directory(file):
    # Set up a temporary directory to store the files generated by widoco
    widoco_temp = os.path.join(currentPath, 'Widoco_temp')

    # if the temporary directory doesn't exist, create it
    if not os.path.exists(widoco_temp):
        os.makedirs(widoco_temp)

    if file:
        # Save the uploaded file to the temporary directory
        filename = secure_filename(file.filename)
        file_path = os.path.join(widoco_temp, filename)
        file.save(file_path)

        # Set up the options for running widoco
        options = ['-ontFile', file_path, '-outFolder', widoco_temp, '-uniteSections', '-rewriteAll']

        # Run widoco using subprocess.run()
        result = subprocess.run(['java', '-jar', 'widoco.jar', *options], check=True)

        # Check if widoco was successful
        if result.returncode == 0 and os.path.exists(widoco_temp):
            # Copy the contents of the widoco_temp directory to the Widoco_Document directory
            widoco_doc_dir = os.path.abspath(os.path.join('..', '..', 'Widoco_Document'))

            # If the Widoco_Document directory doesn't exist, create it
            if not os.path.exists(widoco_doc_dir):
                os.makedirs(widoco_doc_dir)
            else:
                # If it does exist, clean it up before copying the new files
                clean_widoco_document_dir(widoco_doc_dir)
                shutil.copytree(widoco_temp, widoco_doc_dir, dirs_exist_ok=True)
                shutil.rmtree(widoco_temp)
            # Return a response indicating that the documentation was generated successfully
            return jsonify(True)
        else:
            # If widoco was unsuccessful, return a response indicating that something went wrong
            return jsonify('Something went wrong. Please try again later.')
    else:
        # If no file was received, return a response indicating that a file should be uploaded
        return jsonify('No file received. Please upload a file.')
